﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_12___Professor_and_Professor.Repositories;
using Task_12___Professor_and_student.Models;

namespace Task_12___Professor_and_student.Services
{
    public class ProfessorService : IService<Professor>
    {
        ProfessorRepository pr = new ProfessorRepository();
        public void Delete(Professor obj)
        {
            pr.Delete(obj);
        }

        public IEnumerable<Professor> FindAll()
        {
            return pr.FindAll();
        }

        public Professor GetOne(int Id)
        {
            return pr.GetOne(Id);
        }

        public void Save(Professor obj)
        {
            pr.Save(obj);
        }

        public void Update(Professor obj)
        {
            pr.Update(obj);
        }
    }
}
