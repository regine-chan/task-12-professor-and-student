﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_12___Course_and_student.Repositories;
using Task_12___Professor_and_student.Models;
using Task_12___Professor_and_student.Repositories;
using Task_12___StudentCourse_and_student.Repositories;

namespace Task_12___Professor_and_student.Services
{
    public class StudentCourseService : IService<StudentCourse>
    {
        StudentCourseRepository scr = new StudentCourseRepository();

        public void Delete(StudentCourse obj)
        {
            scr.Delete(obj);
        }

        public IEnumerable<StudentCourse> FindAll()
        {
            return scr.FindAll();
        }

        public StudentCourse GetOne(int Id)
        {
            return scr.GetOne(Id);
        }

        public void Save(StudentCourse obj)
        {
            scr.Save(obj);
        }

        public void Update(StudentCourse obj)
        {
            scr.Update(obj);
        }

        
    }
}
