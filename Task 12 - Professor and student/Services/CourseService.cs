﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_12___Course_and_student.Repositories;
using Task_12___Professor_and_student.Models;

namespace Task_12___Professor_and_student.Services
{
    public class CourseService : IService<Course>
    {
        CourseRepository cr = new CourseRepository();

        public void Delete(Course obj)
        {
            cr.Delete(obj);
        }

        public IEnumerable<Course> FindAll()
        {
            return cr.FindAll();
        }

        public Course GetOne(int Id)
        {
            return cr.GetOne(Id);
        }

        public void Save(Course obj)
        {
            cr.Save(obj);
        }

        public void Update(Course obj)
        {
            cr.Update(obj);
        }
    }
}
