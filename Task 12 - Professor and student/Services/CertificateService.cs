﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_12___Professor_and_Certificate.Repositories;
using Task_12___Professor_and_student.Models;

namespace Task_12___Professor_and_student.Services
{
    public class CertificateService : IService<Certificate>
    {
        CertificateRepository cr = new CertificateRepository();
        public void Delete(Certificate obj)
        {
            cr.Delete(obj);
        }

        public IEnumerable<Certificate> FindAll()
        {
            return cr.FindAll();
        }

        public Certificate GetOne(int Id)
        {
            return cr.GetOne(Id);
        }

        public void Save(Certificate obj)
        {
            cr.Save(obj);
        }

        public void Update(Certificate obj)
        {
            cr.Update(obj);
        }
    }
}
