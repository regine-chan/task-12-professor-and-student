﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_12___Professor_and_student.Services
{
    interface IService<T>
    {
        IEnumerable<T> FindAll();

        T GetOne(int Id);

        void Delete(T obj);

        void Save(T obj);

        void Update(T obj);
    }
}
