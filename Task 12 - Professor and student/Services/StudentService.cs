﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Task_12___Professor_and_student.Models;
using Task_12___Professor_and_student.Repositories;
using Task_12___StudentCourse_and_student.Repositories;

namespace Task_12___Professor_and_student.Services
{
    public class StudentService : IService<Student>
    {
        StudentRepository sr = new StudentRepository();

        public IEnumerable<Student> FindAll()
        {
            return sr.FindAll();
        }

        public Student GetOne(int Id)
        {
            return sr.GetOne(Id);
        }

        public void Delete(Student obj)
        {
            sr.Delete(obj);
        }

        public void Save(Student obj)
        {
            sr.Save(obj);
        }

        public void Update(Student obj)
        {
            sr.Update(obj);
        }
    }
}
