﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Task_12___Professor_and_student.Models;
using Task_12___Professor_and_student.Services;
using System.Text.Json;
using System.Diagnostics;

namespace Task_12___Professor_and_student
{
    public partial class Form1 : Form
    {
        // Initializes services to be used
        StudentService studentService = new StudentService();
        ProfessorService professorService = new ProfessorService();
        CourseService courseService = new CourseService();
        CertificateService certificateService = new CertificateService();
        StudentCourseService studentCourseService = new StudentCourseService();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            InitializelbxStudentProfessor();
        }

        // Initializes the first list box with two choices
        private void InitializelbxStudentProfessor()
        {
            lbxStudentProfessor.Items.Add("Student");
            lbxStudentProfessor.Items.Add("Professor");
        }

        // Displays all the professors or students depending on which item one chooses in the lbxStudentProfessor list box
        private void lbxStudentProfessor_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdatePeople();
        }

        // Initializes other listboxes with values relational to the object
        private void lbxPerson_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetViews();
        }

        private void UpdatePeople()
        {
            if (lbxStudentProfessor.SelectedItem.ToString() == "Student")
            {
                Student s = (Student)lbxPerson.SelectedItem;

                lbxPerson.Items.Clear();

                var People = studentService.FindAll();


                if (s != null)
                {
                    foreach (Student student in People)
                    {
                        lbxPerson.Items.Add(student);
                        if (s.Id == student.Id)
                        {
                            lbxPerson.SelectedItem = student;
                            SetViews();
                        }
                    }
                }
                else
                {
                    foreach (Student student in People)
                    {
                        lbxPerson.Items.Add(student);
                    }
                }
            }
            else if (lbxStudentProfessor.SelectedItem.ToString() == "Professor")
            {
                Professor p = (Professor)lbxPerson.SelectedItem;

                lbxPerson.Items.Clear();

                var People = professorService.FindAll();


                if (p != null)
                {
                    foreach (Professor professor in People)
                    {
                        lbxPerson.Items.Add(professor);
                        if (professor.Id == p.Id)
                        {
                            lbxPerson.SelectedItem = professor;
                            SetViews();
                        }
                    }
                }
                else
                {
                    foreach (Professor professor in People)
                    {
                        lbxPerson.Items.Add(professor);
                    }
                }
            }
        }

        private void SetViews()
        {
            // Prints out personal data in a label
            PrintPersonInfo();
            // Sets the other listbox's content
            SetlbxOther();
            // Sets the person listbox's content
            SetlbxPerson();
            // Sets the person's items listbox's others
            SetlbxOthertHas();
            // Sets the person's related people into the person has listbox content
            SetlbxPersonHas();
        }

        // This initializes the other relational table of content of which a student or professor can hold
        // I.e for a student the available courses is listed in the listbox
        // For a professor the available Certificates is listed in the listbox
        private void SetlbxOther()
        {
            lbxAssignOther.Items.Clear();
            if (lbxStudentProfessor.SelectedItem.ToString() == "Student")
            {
                foreach (Course course in courseService.FindAll())
                {
                    lbxAssignOther.Items.Add(course);
                }
            }
            else if (lbxStudentProfessor.SelectedItem.ToString() == "Professor")
            {
                foreach (Certificate certificate in certificateService.FindAll())
                {
                    lbxAssignOther.Items.Add(certificate);
                }
            }
        }

        // This initializes the person relational table of content of which a student or professor can hold
        // I.e for a student the available professors is listed in the listbox
        // For a professor the available students is listed in the listbox
        private void SetlbxPerson()
        {
            lbxAssignPerson.Items.Clear();
            if (lbxStudentProfessor.SelectedItem.ToString() == "Student")
            {
                foreach (Professor professor in professorService.FindAll())
                {
                    lbxAssignPerson.Items.Add(professor);
                }
            }
            else if (lbxStudentProfessor.SelectedItem.ToString() == "Professor")
            {
                foreach (Student student in studentService.FindAll())
                {
                    lbxAssignPerson.Items.Add(student);
                }
            }
        }

        // This initializes the other relational table of content of which a student or professor is holding
        // I.e for a student the current courses is listed in the listbox
        // For a professor the current Certificates is listed in the listbox
        private void SetlbxPersonHas()
        {
            lbxPersonHas.Items.Clear();
            if (lbxStudentProfessor.SelectedItem.ToString() == "Student")
            {
                Student student = (Student)lbxPerson.SelectedItem;
                if (student != null)
                {
                    lbxPersonHas.Items.Add(student.professor);
                }
            }
            else if (lbxStudentProfessor.SelectedItem.ToString() == "Professor")
            {
                Professor professor = (Professor)lbxPerson.SelectedItem;
                if (professor != null)
                {
                    foreach (Student student in professor.Students)
                    {
                        lbxPersonHas.Items.Add(student);
                    }
                }

            }
        }

        // This initializes the other relational table of content of which a student or professor is holding
        // I.e for a student the current professors is listed in the listbox
        // For a professor the current students is listed in the listbox
        private void SetlbxOthertHas()
        {
            lbxOtherHas.Items.Clear();
            if (lbxStudentProfessor.SelectedItem.ToString() == "Student")
            {
                Student student = (Student)lbxPerson.SelectedItem;
                if (student != null)
                {
                    foreach (StudentCourse studentCourse in student.StudentCourses)
                    {
                        lbxOtherHas.Items.Add(studentCourse);
                    }
                }
            }
            else if (lbxStudentProfessor.SelectedItem.ToString() == "Professor")
            {
                Professor professor = (Professor)lbxPerson.SelectedItem;
                if (professor != null)
                {
                    if (professor.certificate != null)
                    {
                        lbxOtherHas.Items.Add(professor.certificate);
                    }
                }

            }
        }

        // A function to print student or professor information on the far right, as the attributes are different
        private void PrintPersonInfo()
        {
            if (lbxStudentProfessor.SelectedItem.ToString() == "Student")
            {
                // Prints student info
                PrintStudentInfo();
            }
            else if (lbxStudentProfessor.SelectedItem.ToString() == "Professor")
            {
                // Prints professor info
                PrintProfessorInfo();
            }
        }

        // Builds a string with the professors attributes and displays it
        // Update: Now serializes the object and prints it
        private void PrintProfessorInfo()
        {
            Professor p = (Professor)lbxPerson.SelectedItem;
            /*
            StringBuilder sb = new StringBuilder();
            sb.Append($"Name: {p.Name}\n");
            sb.Append($"Gender: {p.Gender}\n");
            lbInfo.Text = sb.ToString();
            */
            string jsonString;
            try
            {
                if (p != null)
                {
                    jsonString = JsonSerializer.Serialize(p);
                    jsonString = jsonString.Replace(",", "\n");
                    lbInfo.Text = jsonString;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Couldn't serialize json \n" + ex.Message);
            }


        }

        // Builds a string with the students attributes and displays it
        // Update: Now serializes the object and prints it
        private void PrintStudentInfo()
        {
            Student s = (Student)lbxPerson.SelectedItem;
            /*
            if (s != null)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append($"Name: {s.Name}\n");
                sb.Append($"Gender: {s.Gender}\n");
                sb.Append($"Age: {s.Age}\n");
                lbInfo.Text = sb.ToString();
            }
            */
            string jsonString;
            try
            {
                if (s != null)
                {
                    jsonString = JsonSerializer.Serialize(s);
                    jsonString = jsonString.Replace(",", "\n");
                    lbInfo.Text = jsonString;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Couldn't serialize json \n" + ex.Message);
            }

        }

        // Handles on link person clicks for students and professors
        // If a student is selected, a professor is linked to the selected student
        // If a professor is selected, a student is linked to the selected professor
        private void btLinkPerson_Click(object sender, EventArgs e)
        {
            if (lbxAssignPerson.SelectedItem != null)
            {
                try
                {
                    if (lbxStudentProfessor.SelectedItem.ToString() == "Student")
                    {
                        Student student = (Student)lbxPerson.SelectedItem;
                        if (student != null)
                        {
                            student.professor = (Professor)lbxAssignPerson.SelectedItem;
                            if (student.professor != null)
                            {
                                studentService.Update(student);
                            }
                        }
                    }
                    else if (lbxStudentProfessor.SelectedItem.ToString() == "Professor")
                    {
                        Professor professor = (Professor)lbxPerson.SelectedItem;
                        if (professor != null)
                        {
                            if (lbxAssignPerson != null)
                            {
                                professor.Students.Add((Student)lbxAssignPerson.SelectedItem);
                                professorService.Update(professor);
                            }
                        }

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Can't have more than one person linked\n" + ex.Message);
                }
                UpdatePeople();

            }
        }

        // Ulinks the selected person from the other selected person
        // If a student is viewed and the user selects the student's current professor. The professor is unlinked from the student.
        // If a professor is viewed and the user selects a student under the professors supervision. The student is unlinked from the professor.
        private void btUnlinkPerson_Click(object sender, EventArgs e)
        {
            if (lbxPersonHas.SelectedItem != null)
            {
                try
                {
                    if (lbxStudentProfessor.SelectedItem.ToString() == "Student")
                    {
                        Student student = (Student)lbxPerson.SelectedItem;
                        if (student != null)
                        {
                            student.professor = null;
                            studentService.Update(student);
                        }
                    }
                    else if (lbxStudentProfessor.SelectedItem.ToString() == "Professor")
                    {
                        Professor professor = (Professor)lbxPerson.SelectedItem;
                        if (professor != null)
                        {
                            if (lbxAssignPerson.SelectedItem != null)
                            {
                                professor.Students.Remove((Student)lbxAssignPerson.SelectedItem);
                                professorService.Update(professor);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Can't have more than one person linked \n" + ex.Message);
                }
                UpdatePeople();
            }
        }

        // This links a certificate or course the selected student or professor object
        private void btLinkOther_Click(object sender, EventArgs e)
        {
            if (lbxAssignOther.SelectedItem != null)
            {
                try
                {
                    if (lbxStudentProfessor.SelectedItem.ToString() == "Student")
                    {
                        Student student = (Student)lbxPerson.SelectedItem;
                        if (student != null)
                        {
                            if (lbxAssignOther.SelectedItem != null)
                            {
                                student.StudentCourses.Add(new StudentCourse { Student = student, Course = (Course)lbxAssignOther.SelectedItem });
                                // studentCourseService.Save(new StudentCourse { Student = student, Course = (Course)lbxAssignOther.SelectedItem });
                                studentService.Update(student);
                            }
                        }
                    }
                    else if (lbxStudentProfessor.SelectedItem.ToString() == "Professor")
                    {
                        Professor professor = (Professor)lbxPerson.SelectedItem;
                        if (professor != null)
                        {
                            if (lbxAssignOther != null)
                            {
                                professor.certificate = (Certificate)lbxAssignOther.SelectedItem;
                                professorService.Update(professor);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Can't link other \n" + ex.Message);
                }
                UpdatePeople();
            }
        }

        // This unlinks a certificate or a course on the selected student or professor object
        private void btUnlinkOther_Click(object sender, EventArgs e)
        {
            if (lbxOtherHas.SelectedItem != null)
            {
                try
                {
                    if (lbxStudentProfessor.SelectedItem.ToString() == "Student")
                    {
                        StudentCourse studentCourse = (StudentCourse)lbxOtherHas.SelectedItem;
                        if (studentCourse != null)
                        {
                            studentCourseService.Delete(studentCourse);

                        }
                    }
                    else if (lbxStudentProfessor.SelectedItem.ToString() == "Professor")
                    {
                        Professor professor = (Professor)lbxPerson.SelectedItem;
                        if (professor != null)
                        {
                            professor.certificate = null;
                            professorService.Update(professor);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Can't ulink other \n" + ex.Message);
                }
                UpdatePeople();

            }
        }



    }
}
