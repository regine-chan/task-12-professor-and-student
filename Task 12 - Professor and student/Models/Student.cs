﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Task_12___Professor_and_student.Models
{
    public class Student
    {
        // Primary key
        public int Id { get; set; }

        public string Name { get; set; }

        public int Age { get; set; }

        public string Gender { get; set; }

        // Foreign key
        public int? ProfessorId { get; set; }

        // Direction to otm relationship
        public Professor professor { get; set; }

        // Direction to mtm relationship
        [JsonIgnore]
        public ICollection<StudentCourse> StudentCourses { get; set; }

    }
}
