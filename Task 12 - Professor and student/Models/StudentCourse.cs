﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_12___Professor_and_student.Models
{
    public class StudentCourse
    {
        // Foreign key
        public int StudentId { get; set; }

        // Direction otm relationship
        public Student Student { get; set; }

        // Foreign key
        public int CourseId { get; set; }

        // Direction otm relationship
        public Course Course { get; set; }
    }
}
