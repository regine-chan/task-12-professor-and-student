﻿using System.Collections;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Task_12___Professor_and_student.Models
{
    public class Professor
    {
        // Primary key
        public int Id { get; set; }

        public string Name { get; set; }

        public string Gender { get; set; }

        // Foreign key
        public int? CertificateId { get; set; }

        // Direction to oto relationship
        public Certificate certificate { get; set; }

        // Direction to otm relationship
        [JsonIgnore]
        public ICollection<Student> Students { get; set; }

    }
}