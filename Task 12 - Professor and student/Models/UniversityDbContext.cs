﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_12___Professor_and_student.Models
{
    public class UniversityDbContext : DbContext
    {
        public DbSet<Professor> Professors { get; set; }

        public DbSet<Student> Students { get; set; }

        public DbSet<Certificate> Certifications { get; set; }

        public DbSet<Course> Courses { get; set; }

        public DbSet<StudentCourse> StudentCourses { get; set; }
        public object Certificates { get; internal set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=PC7261\\SQLEXPRESS;Initial Catalog=UniversityDB;Integrated Security=True");
            optionsBuilder.EnableSensitiveDataLogging();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<StudentCourse>().HasKey(sc => new { sc.StudentId, sc.CourseId });

            modelBuilder.Entity<Certificate>().HasData(new Certificate { Id = 1, Name = "Ph.d Physics" });
            modelBuilder.Entity<Certificate>().HasData(new Certificate { Id = 2, Name = "Ph.d Chemistry" });
            modelBuilder.Entity<Certificate>().HasData(new Certificate { Id = 3, Name = "Ph.d Database Architecture" });
            modelBuilder.Entity<Certificate>().HasData(new Certificate { Id = 4, Name = "Ph.d Mathematics" });
            modelBuilder.Entity<Certificate>().HasData(new Certificate { Id = 5, Name = "Ph.d Biology" });
            modelBuilder.Entity<Certificate>().HasData(new Certificate { Id = 6, Name = "Ph.d Gymnastics" });
            modelBuilder.Entity<Certificate>().HasData(new Certificate { Id = 7, Name = "Ph.d Cooking" });

            modelBuilder.Entity<Student>().HasData(new Student { Id = 1, Name = "Student1", Gender = "Male", Age = 19 });
            modelBuilder.Entity<Student>().HasData(new Student { Id = 2, Name = "Student2", Gender = "Female", Age = 22 });
            modelBuilder.Entity<Student>().HasData(new Student { Id = 3, Name = "Student3", Gender = "Male", Age = 21 });
            modelBuilder.Entity<Student>().HasData(new Student { Id = 4, Name = "Student4", Gender = "Female", Age = 24 });
            modelBuilder.Entity<Student>().HasData(new Student { Id = 5, Name = "Student5", Gender = "Female", Age = 20 });
            modelBuilder.Entity<Student>().HasData(new Student { Id = 6, Name = "Prodigy", Gender = "Non-Binary", Age = 12 });
            modelBuilder.Entity<Student>().HasData(new Student { Id = 7, Name = "Student7", Gender = "Non-Binary", Age = 52 });

            modelBuilder.Entity<Professor>().HasData(new Professor { Id = 1, Name = "Professor1", Gender = "Male" });
            modelBuilder.Entity<Professor>().HasData(new Professor { Id = 2, Name = "Professor2", Gender = "Female" });
            modelBuilder.Entity<Professor>().HasData(new Professor { Id = 3, Name = "Professor3", Gender = "Non-Binary" });
            modelBuilder.Entity<Professor>().HasData(new Professor { Id = 4, Name = "Professor4", Gender = "Non-Binary" });
            modelBuilder.Entity<Professor>().HasData(new Professor { Id = 5, Name = "Professor5", Gender = "Female" });
            modelBuilder.Entity<Professor>().HasData(new Professor { Id = 6, Name = "Professor6", Gender = "Male" });





            modelBuilder.Entity<Course>().HasData(new Course { Id = 1, Name = "Mathematics 1" });
            modelBuilder.Entity<Course>().HasData(new Course { Id = 2, Name = "Mathematics 2" });
            modelBuilder.Entity<Course>().HasData(new Course { Id = 3, Name = "Mathematics 3" });
            modelBuilder.Entity<Course>().HasData(new Course { Id = 4, Name = "Mathematics 4" });
            modelBuilder.Entity<Course>().HasData(new Course { Id = 5, Name = "Physics 1" });
            modelBuilder.Entity<Course>().HasData(new Course { Id = 6, Name = "Physics 2" });
            modelBuilder.Entity<Course>().HasData(new Course { Id = 7, Name = "Chemistry 1" });
        }
    }
}
