﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Task_12___Professor_and_student.Models
{
    public class Course
    {
        // Primary key
        public int Id { get; set; }

        public string Name { get; set; }

        // Direction to mtm relationship
        [JsonIgnore]
        public ICollection<StudentCourse> StudentCourses { get; set; }
    }
}