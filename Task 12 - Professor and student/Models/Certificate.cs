﻿using System.Text.Json.Serialization;

namespace Task_12___Professor_and_student.Models
{
    public class Certificate
    {
        // Primary key     
        public int Id { get; set; }

        public string Name { get; set; }

        // Direction to oto relationship
        [JsonIgnore]
        public Professor Professor { get; set; }


    }
}