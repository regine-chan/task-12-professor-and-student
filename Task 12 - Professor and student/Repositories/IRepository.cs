﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_12___Professor_and_student.Repositories
{
    interface IRepository<T>
    {
        IEnumerable<T> FindAll();

        T GetOne(int Id);

        void Save(T obj);

        void Update(T obj);

        void Delete(T obj);
    }
}
