﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_12___Professor_and_student.Models;

namespace Task_12___Professor_and_student.Repositories
{
    public class StudentRepository : IRepository<Student>
    {
        public void Delete(Student obj)
        {
            try
            {
                using (UniversityDbContext ctx = new UniversityDbContext())
                {
                    ctx.Students.Remove(obj);
                    ctx.SaveChanges();
                }
            }
            catch (DbUpdateConcurrencyException ex)
            {
                // Do nothing
            }
        }

        public IEnumerable<Student> FindAll()
        {
            using (UniversityDbContext ctx = new UniversityDbContext())

                return ctx.Students.Include(s => s.professor).Include(s => s.StudentCourses).ThenInclude(c => c.Course).ToList();
        }

        public Student GetOne(int Id)
        {
            using (UniversityDbContext ctx = new UniversityDbContext())

                return ctx.Students.Find(Id);
        }

        public void Save(Student obj)
        {
            using (UniversityDbContext ctx = new UniversityDbContext())
            {
                ctx.Students.Add(obj);
                ctx.SaveChanges();
            }
        }

        public void Update(Student obj)
        {
            using (UniversityDbContext ctx = new UniversityDbContext())
            {
                ctx.Students.Update(obj);
                ctx.SaveChanges();
            }
        }

    }
}
