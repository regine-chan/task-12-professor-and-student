﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_12___Professor_and_student.Models;
using Task_12___Professor_and_student.Repositories;

namespace Task_12___Course_and_student.Repositories
{
    public class CourseRepository : IRepository<Course>
    {
        public void Delete(Course obj)
        {
            try
            {
                using (UniversityDbContext ctx = new UniversityDbContext())
                {
                    ctx.Courses.Remove(obj);
                    ctx.SaveChanges();
                }
            }
            catch (DbUpdateConcurrencyException ex)
            {
                // Do nothing
            }
        }

        public IEnumerable<Course> FindAll()
        {
            using (UniversityDbContext ctx = new UniversityDbContext())
            {
                return ctx.Courses.ToList();
            }
        }

        public Course GetOne(int Id)
        {
            using (UniversityDbContext ctx = new UniversityDbContext())

                return ctx.Courses.Find(Id);
        }

        public void Save(Course obj)
        {
            using (UniversityDbContext ctx = new UniversityDbContext())
            {
                ctx.Courses.Add(obj);
                ctx.SaveChanges();
            }
        }

        public void Update(Course obj)
        {
            using (UniversityDbContext ctx = new UniversityDbContext())
            {
                ctx.Courses.Update(obj);
                ctx.SaveChanges();
            }
        }
    }
}
