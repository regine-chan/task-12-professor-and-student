﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_12___Professor_and_student.Models;
using Task_12___Professor_and_student.Repositories;

namespace Task_12___StudentCourse_and_student.Repositories
{
    public class StudentCourseRepository : IRepository<StudentCourse>
    {
        public void Delete(StudentCourse obj)
        {
            try
            {
                using (UniversityDbContext ctx = new UniversityDbContext())
                {
                    ctx.StudentCourses.Remove(obj);
                    ctx.SaveChanges();
                }
            }
            catch (DbUpdateConcurrencyException ex)
            {
                // Do nothing
            }
        }

        public IEnumerable<StudentCourse> FindAll()
        {
            using (UniversityDbContext ctx = new UniversityDbContext())

                return ctx.StudentCourses.Include(sc => sc.Student).Include(sc => sc.Course).ToList();
        }

        public StudentCourse GetOne(int Id)
        {
            using (UniversityDbContext ctx = new UniversityDbContext())

                return ctx.StudentCourses.Find(Id);
        }

        public void Save(StudentCourse obj)
        {
            using (UniversityDbContext ctx = new UniversityDbContext())
            {
                ctx.StudentCourses.Add(obj);
                ctx.SaveChanges();
            }
        }

        public void Update(StudentCourse obj)
        {
            using (UniversityDbContext ctx = new UniversityDbContext())
            {
                ctx.StudentCourses.Update(obj);
                ctx.SaveChanges();
            }
        }

    }
}
