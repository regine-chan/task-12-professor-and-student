﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;
using Task_12___Professor_and_student.Models;
using Task_12___Professor_and_student.Repositories;

namespace Task_12___Professor_and_Professor.Repositories
{
    public class ProfessorRepository : IRepository<Professor>
    {
        public void Delete(Professor obj)
        {
            try
            {
                using (UniversityDbContext ctx = new UniversityDbContext())
                {
                    ctx.Professors.Remove(obj);
                    ctx.SaveChanges();
                }
            }
            catch (DbUpdateConcurrencyException ex)
            {
                // DO nothing
            }
        }

        public IEnumerable<Professor> FindAll()
        {
            using (UniversityDbContext ctx = new UniversityDbContext())

                return ctx.Professors.Include(p => p.Students).Include(p => p.certificate).ToList();
        }

        public Professor GetOne(int Id)
        {
            using (UniversityDbContext ctx = new UniversityDbContext())

                return ctx.Professors.Find(Id);
        }

        public void Save(Professor obj)
        {
            using (UniversityDbContext ctx = new UniversityDbContext())
            {
                ctx.Professors.Add(obj);
                ctx.SaveChanges();
            }
        }

        public void Update(Professor obj)
        {
            using (UniversityDbContext ctx = new UniversityDbContext())
            {
                ctx.Professors.Update(obj);
                ctx.SaveChanges();
            }
        }
    }
}
