﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Task_12___Professor_and_student.Models;
using Task_12___Professor_and_student.Repositories;

namespace Task_12___Professor_and_Certificate.Repositories
{
    public class CertificateRepository : IRepository<Certificate>
    {
        public void Delete(Certificate obj)
        {
            try
            {
                using (UniversityDbContext ctx = new UniversityDbContext())
                {
                    ctx.Certifications.Remove(obj);
                    ctx.SaveChanges();
                }
            }
            catch (DbUpdateConcurrencyException ex)
            {
                // Do nothing
            }
        }

        public IEnumerable<Certificate> FindAll()
        {
            using (UniversityDbContext ctx = new UniversityDbContext())
            {
                return ctx.Certifications.ToList();
            }
        }

        public Certificate GetOne(int Id)
        {
            using (UniversityDbContext ctx = new UniversityDbContext())
            {
                return ctx.Certifications.Find(Id);
            }
        }

        public void Save(Certificate obj)
        {
            using (UniversityDbContext ctx = new UniversityDbContext())
            {
                ctx.Certifications.Add(obj);
                ctx.SaveChanges();
            }
        }

        public void Update(Certificate obj)
        {
            using (UniversityDbContext ctx = new UniversityDbContext())
            {
                ctx.Certifications.Update(obj);
                ctx.SaveChanges();
            }
        }
    }
}
