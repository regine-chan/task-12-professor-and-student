﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Task_12___Professor_and_student.Migrations
{
    public partial class Initialmigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Certifications",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Certifications", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Courses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Courses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Professors",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    CertificateId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Professors", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Professors_Certifications_CertificateId",
                        column: x => x.CertificateId,
                        principalTable: "Certifications",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Students",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Age = table.Column<int>(nullable: false),
                    Gender = table.Column<string>(nullable: true),
                    ProfessorId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Students", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Students_Professors_ProfessorId",
                        column: x => x.ProfessorId,
                        principalTable: "Professors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "StudentCourses",
                columns: table => new
                {
                    StudentId = table.Column<int>(nullable: false),
                    CourseId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentCourses", x => new { x.StudentId, x.CourseId });
                    table.ForeignKey(
                        name: "FK_StudentCourses_Courses_CourseId",
                        column: x => x.CourseId,
                        principalTable: "Courses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentCourses_Students_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Students",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Certifications",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Ph.d Physics" },
                    { 2, "Ph.d Chemistry" },
                    { 3, "Ph.d Database Architecture" },
                    { 4, "Ph.d Mathematics" },
                    { 5, "Ph.d Biology" },
                    { 6, "Ph.d Gymnastics" },
                    { 7, "Ph.d Cooking" }
                });

            migrationBuilder.InsertData(
                table: "Courses",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 6, "Physics 2" },
                    { 5, "Physics 1" },
                    { 4, "Mathematics 4" },
                    { 7, "Chemistry 1" },
                    { 2, "Mathematics 2" },
                    { 1, "Mathematics 1" },
                    { 3, "Mathematics 3" }
                });

            migrationBuilder.InsertData(
                table: "Professors",
                columns: new[] { "Id", "CertificateId", "Gender", "Name" },
                values: new object[,]
                {
                    { 1, null, "Male", "Professor1" },
                    { 2, null, "Female", "Professor2" },
                    { 3, null, "Non-Binary", "Professor3" },
                    { 4, null, "Non-Binary", "Professor4" },
                    { 5, null, "Female", "Professor5" },
                    { 6, null, "Male", "Professor6" }
                });

            migrationBuilder.InsertData(
                table: "Students",
                columns: new[] { "Id", "Age", "Gender", "Name", "ProfessorId" },
                values: new object[,]
                {
                    { 6, 12, "Non-Binary", "Prodigy", null },
                    { 1, 19, "Male", "Student1", null },
                    { 2, 22, "Female", "Student2", null },
                    { 3, 21, "Male", "Student3", null },
                    { 4, 24, "Female", "Student4", null },
                    { 5, 20, "Female", "Student5", null },
                    { 7, 52, "Non-Binary", "Student7", null }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Professors_CertificateId",
                table: "Professors",
                column: "CertificateId",
                unique: true,
                filter: "[CertificateId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_StudentCourses_CourseId",
                table: "StudentCourses",
                column: "CourseId");

            migrationBuilder.CreateIndex(
                name: "IX_Students_ProfessorId",
                table: "Students",
                column: "ProfessorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StudentCourses");

            migrationBuilder.DropTable(
                name: "Courses");

            migrationBuilder.DropTable(
                name: "Students");

            migrationBuilder.DropTable(
                name: "Professors");

            migrationBuilder.DropTable(
                name: "Certifications");
        }
    }
}
