﻿namespace Task_12___Professor_and_student
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbxStudentProfessor = new System.Windows.Forms.ListBox();
            this.lbxPerson = new System.Windows.Forms.ListBox();
            this.lbxAssignPerson = new System.Windows.Forms.ListBox();
            this.lbxAssignOther = new System.Windows.Forms.ListBox();
            this.lbInfo = new System.Windows.Forms.Label();
            this.lbxOtherHas = new System.Windows.Forms.ListBox();
            this.lbxPersonHas = new System.Windows.Forms.ListBox();
            this.btLinkPerson = new System.Windows.Forms.Button();
            this.btUnlinkPerson = new System.Windows.Forms.Button();
            this.btLinkOther = new System.Windows.Forms.Button();
            this.btUnlinkOther = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbxStudentProfessor
            // 
            this.lbxStudentProfessor.FormattingEnabled = true;
            this.lbxStudentProfessor.Location = new System.Drawing.Point(30, 166);
            this.lbxStudentProfessor.Name = "lbxStudentProfessor";
            this.lbxStudentProfessor.Size = new System.Drawing.Size(141, 95);
            this.lbxStudentProfessor.TabIndex = 0;
            this.lbxStudentProfessor.SelectedIndexChanged += new System.EventHandler(this.lbxStudentProfessor_SelectedIndexChanged);
            // 
            // lbxPerson
            // 
            this.lbxPerson.DisplayMember = "Name";
            this.lbxPerson.FormattingEnabled = true;
            this.lbxPerson.Location = new System.Drawing.Point(226, 49);
            this.lbxPerson.Name = "lbxPerson";
            this.lbxPerson.Size = new System.Drawing.Size(120, 329);
            this.lbxPerson.TabIndex = 1;
            this.lbxPerson.SelectedIndexChanged += new System.EventHandler(this.lbxPerson_SelectedIndexChanged);
            // 
            // lbxAssignPerson
            // 
            this.lbxAssignPerson.DisplayMember = "Name";
            this.lbxAssignPerson.FormattingEnabled = true;
            this.lbxAssignPerson.Location = new System.Drawing.Point(395, 49);
            this.lbxAssignPerson.Name = "lbxAssignPerson";
            this.lbxAssignPerson.Size = new System.Drawing.Size(120, 134);
            this.lbxAssignPerson.TabIndex = 2;
            // 
            // lbxAssignOther
            // 
            this.lbxAssignOther.DisplayMember = "Name";
            this.lbxAssignOther.FormattingEnabled = true;
            this.lbxAssignOther.Location = new System.Drawing.Point(395, 231);
            this.lbxAssignOther.Name = "lbxAssignOther";
            this.lbxAssignOther.Size = new System.Drawing.Size(120, 147);
            this.lbxAssignOther.TabIndex = 3;
            // 
            // lbInfo
            // 
            this.lbInfo.AutoSize = true;
            this.lbInfo.Location = new System.Drawing.Point(675, 203);
            this.lbInfo.Name = "lbInfo";
            this.lbInfo.Size = new System.Drawing.Size(25, 13);
            this.lbInfo.TabIndex = 4;
            this.lbInfo.Text = "Info";
            // 
            // lbxOtherHas
            // 
            this.lbxOtherHas.DisplayMember = "Course.Name";
            this.lbxOtherHas.FormattingEnabled = true;
            this.lbxOtherHas.Location = new System.Drawing.Point(533, 231);
            this.lbxOtherHas.Name = "lbxOtherHas";
            this.lbxOtherHas.Size = new System.Drawing.Size(120, 147);
            this.lbxOtherHas.TabIndex = 6;
            // 
            // lbxPersonHas
            // 
            this.lbxPersonHas.DisplayMember = "Name";
            this.lbxPersonHas.FormattingEnabled = true;
            this.lbxPersonHas.Location = new System.Drawing.Point(533, 49);
            this.lbxPersonHas.Name = "lbxPersonHas";
            this.lbxPersonHas.Size = new System.Drawing.Size(120, 134);
            this.lbxPersonHas.TabIndex = 5;
            // 
            // btLinkPerson
            // 
            this.btLinkPerson.Location = new System.Drawing.Point(416, 193);
            this.btLinkPerson.Name = "btLinkPerson";
            this.btLinkPerson.Size = new System.Drawing.Size(75, 23);
            this.btLinkPerson.TabIndex = 7;
            this.btLinkPerson.Text = "Link";
            this.btLinkPerson.UseVisualStyleBackColor = true;
            this.btLinkPerson.Click += new System.EventHandler(this.btLinkPerson_Click);
            // 
            // btUnlinkPerson
            // 
            this.btUnlinkPerson.Location = new System.Drawing.Point(561, 193);
            this.btUnlinkPerson.Name = "btUnlinkPerson";
            this.btUnlinkPerson.Size = new System.Drawing.Size(75, 23);
            this.btUnlinkPerson.TabIndex = 8;
            this.btUnlinkPerson.Text = "Unlink";
            this.btUnlinkPerson.UseVisualStyleBackColor = true;
            this.btUnlinkPerson.Click += new System.EventHandler(this.btUnlinkPerson_Click);
            // 
            // btLinkOther
            // 
            this.btLinkOther.Location = new System.Drawing.Point(416, 384);
            this.btLinkOther.Name = "btLinkOther";
            this.btLinkOther.Size = new System.Drawing.Size(75, 23);
            this.btLinkOther.TabIndex = 9;
            this.btLinkOther.Text = "Link";
            this.btLinkOther.UseVisualStyleBackColor = true;
            this.btLinkOther.Click += new System.EventHandler(this.btLinkOther_Click);
            // 
            // btUnlinkOther
            // 
            this.btUnlinkOther.Location = new System.Drawing.Point(561, 384);
            this.btUnlinkOther.Name = "btUnlinkOther";
            this.btUnlinkOther.Size = new System.Drawing.Size(75, 23);
            this.btUnlinkOther.TabIndex = 10;
            this.btUnlinkOther.Text = "Unlink";
            this.btUnlinkOther.UseVisualStyleBackColor = true;
            this.btUnlinkOther.Click += new System.EventHandler(this.btUnlinkOther_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btUnlinkOther);
            this.Controls.Add(this.btLinkOther);
            this.Controls.Add(this.btUnlinkPerson);
            this.Controls.Add(this.btLinkPerson);
            this.Controls.Add(this.lbxOtherHas);
            this.Controls.Add(this.lbxPersonHas);
            this.Controls.Add(this.lbInfo);
            this.Controls.Add(this.lbxAssignOther);
            this.Controls.Add(this.lbxAssignPerson);
            this.Controls.Add(this.lbxPerson);
            this.Controls.Add(this.lbxStudentProfessor);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lbxStudentProfessor;
        private System.Windows.Forms.ListBox lbxPerson;
        private System.Windows.Forms.ListBox lbxAssignPerson;
        private System.Windows.Forms.ListBox lbxAssignOther;
        private System.Windows.Forms.Label lbInfo;
        private System.Windows.Forms.ListBox lbxOtherHas;
        private System.Windows.Forms.ListBox lbxPersonHas;
        private System.Windows.Forms.Button btLinkPerson;
        private System.Windows.Forms.Button btUnlinkPerson;
        private System.Windows.Forms.Button btLinkOther;
        private System.Windows.Forms.Button btUnlinkOther;
    }
}

