To run this application open its .sln in visual studio 2019.

Ensure you have MySQL server installed and edited the connection string in UniversitydbContext

Open nuget packet manager console and type - add-migration <migration name>

Then type update-database

Run the application

Here you can link professors to students
Certificate to professors
Courses to students

One major bug crashes the program with dbupdateconcurrencyexception
Seems like transactions are not closed before the next begins (?)

If you can find the bug then please let me know ...